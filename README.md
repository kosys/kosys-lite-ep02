コウシス！ LITE #2
===================

**「さようなら7」**

情報セキュリティ普及啓発ミニアニメ「コウシス！LITE」シリーズ第2弾。

## 概要

これはオープンソースアニメーション作品「コウシス！LITE #2」の制作用のリポジトリです。


## ライセンス
* [CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)
* [CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)

一部異なるライセンスのファイルも含まれています。詳細はLICENSE.txtをご覧ください。

## クレジット
Copyright (C) OPAP-JP contributors. (https://opap.jp/contributors )

この作品は[CC-BY 4.0](http://creativecommons.org/licenses/by/4.0/)および[CC-BY 2.1 JP](http://creativecommons.org/licenses/by/2.1/jp/)のもとに提供されています。
この作品をご利用の際には、適切なクレジットの表示が必要です。クレジットや著作権についての詳細は [こうしす！クレジット](https://opap.jp/contributors/) をご覧ください。

